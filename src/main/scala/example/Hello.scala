package example
import java.io._

object Hello extends App {
  def sayHello(size: Int, numbers: List[Int]) = {
    if(size < 1) {
      println("El tamaño no debe ser menor a 1")
    } else {
      val superList = readingNumbers(size - 1, numbers)
      writingNumbers(g(superList, List()))
    }
  }

  def readingNumbers(size: Int, numbers: List[Int]): List[List[List[Int]]] = {
    val results = numbers.map(n => {
      val guide = numberGuide(n)
      expansionAll(guide, size)
    })
    results
  }

  def numberGuide(number: Int) : List[List[Int]]  = {
    val representation = number match {
      case 1 => List(List(0,0,0), List(0,0,3), List(0,0,0), List(0,0,3), List(0,0,0))
      case 2 => List(List(0,1,0), List(0,0,3), List(0,1,0), List(3,0,0), List(0,1,0))
      case 3 => List(List(0,1,0), List(0,0,3), List(0,1,0), List(0,0,3), List(0,1,0))
      case 4 => List(List(0,0,0), List(3,0,3), List(0,1,0), List(0,0,3), List(0,0,0))
      case 5 => List(List(0,1,0), List(3,0,0), List(0,1,0), List(0,0,3), List(0,1,0))
      case 6 => List(List(0,1,0), List(3,0,0), List(0,1,0), List(3,0,3), List(0,1,0))
      case 7 => List(List(0,1,0), List(0,0,3), List(0,0,0), List(0,0,3), List(0,0,0))
      case 8 => List(List(0,1,0), List(3,0,3), List(0,1,0), List(3,0,3), List(0,1,0))
      case 9 => List(List(0,1,0), List(3,0,3), List(0,1,0), List(0,0,3), List(0,1,0))
    }
    representation
  }

  def expansionHorizontally(l: List[List[Int]], size: Int) = {
    l.map{ elem =>
      {
        val el = List.fill(size)(elem(1))
        insert(elem,2,el)
      }
    }
  }

  def expansionVertically(listOfLists: List[List[Int]], size: Int) = {
    def insertExpansionVertically = {
      val f = List.fill(size)(listOfLists(1))
      val result = insert(listOfLists, 2, f)

      val g = List.fill(size)(listOfLists(3))
      insert(result, 4 + size,g)
    }
    insertExpansionVertically
  }

  def expansionAll(numGuide: List[List[Int]], size: Int) = {
    val e = expansionVertically(numGuide, size)
    expansionHorizontally(e, size)
  }

  def insert[T](list: List[T], i: Int, values: List[T]) = {
    val (front, back) = list.splitAt(i)
    front ++ values ++ back
  }

  def writingNumbers(number: List[Int]) = {
    val s = new PrintWriter("test.txt")

    def outputNumber = number.map(e => {
      val w = e match {
        case 3 => "|"
        case 1 => "_"
        case 0 => " " 
        case -1 => "\n"
      }
      w
    }).mkString("")
    s.write(outputNumber)
    s.close()
  }

  def g(lists: List[List[List[Int]]], r: List[Int]): List[Int] = {
    if(lists.head.isEmpty) {
      r 
    } else {
      val result = lists.map(l => {
        l.head :+ 0
      }).flatMap(el => el)
      
      val list = lists.map(l => l.tail)
      g(list, r ++ result :+ -1)
    }
  }
}

