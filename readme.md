- Para correr el programa: 

1. clonar el proyecto 

2. instalar sbt

3. en la raiz del proyecto por medio de la consola ingresar el comado "sbt" 

4. despues de descargar los plugins necesarios, ingresar el comando "console"

5. En la consola de scala importar la clase : scala> import example.Hello

6. Ejecutar el programa de la siguiente manera scala> Hello.sayHello(1, List(1,2,3,4,5,6,7,8,9)) donde el primer parametro es el tama�o y el segundo una lista con los digitos.

7. El programa sale como output en un archivo llamado test.txt en la raiz del proyecto.


